﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace ParallelCalculations
{
    internal class Program
    {
        delegate long Calculation(int[] arr);
        static void Main(string[] args)
        {
            var length = 100_000;
            var arr = Randomize.GetRandomizeArray(length);

            Calculate("Последовательное вычисление", arr, SequentialAddition);
            Calculate("Параллельное с помощью LINQ", arr, LinqPAddition);
            Calculate("Параллельное с помощью ThreadPool", arr, ParallelAddition);

            Console.WriteLine();

            length = 1_000_000;
            arr = Randomize.GetRandomizeArray(length);

            Calculate("Последовательное вычисление", arr, SequentialAddition);
            Calculate("Параллельное с помощью LINQ", arr, LinqPAddition);
            Calculate("Параллельное с помощью ThreadPool", arr, ParallelAddition);

            Console.WriteLine();

            length = 10_000_000;
            arr = Randomize.GetRandomizeArray(length);

            Calculate("Последовательное вычисление", arr, SequentialAddition);
            Calculate("Параллельное с помощью LINQ", arr, LinqPAddition);
            Calculate("Параллельное с помощью ThreadPool", arr, ParallelAddition);

        }

        private static void Calculate(string name, int[] arr, Calculation calculation)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            var sum = calculation.Invoke(arr);

            stopWatch.Stop();

            Console.WriteLine($"Результат: {sum}");

            Console.WriteLine($"{name}, длина: {arr.Length.ToString("N0")}, время: {stopWatch.Elapsed}");
        }
        private static long SequentialAddition(int[] arr)
        {
            long res = 0;
            foreach (var a in arr)
                res += a;

            return res;
        }

        private static long LinqPAddition(int[] arr)
        {
            var sum = arr.AsParallel().Sum();
            return sum;
        }

        private static long ParallelAddition(int[] arr)
        {
            long res = 0;
            const int threadsCount = 8;

            var chunkLength = (int)Math.Ceiling(arr.Length / (double)threadsCount);

            var parts = Enumerable.Range(0, threadsCount)
                .Select(i => arr.Skip(i * chunkLength).Take(chunkLength)).ToArray();

            var doneEvents = new ManualResetEvent[parts.Count()];
            var resArrCalcs = new ThreadPoolCalculating[parts.Count()];

            var nextIndex = 0;
            for (var i = 0; i < parts.Count(); i++)
            {
                doneEvents[i] = new ManualResetEvent(false);
                resArrCalcs[i] = new ThreadPoolCalculating(arr, nextIndex, parts[i].Count(), doneEvents[i]);
                ThreadPool.QueueUserWorkItem(resArrCalcs[i].ThreadPoolCallback, i);
                nextIndex += parts[i].Count();
            }

            WaitHandle.WaitAll(doneEvents);

            foreach (var r in resArrCalcs)
                res += r.Result;

            return res;
        }
    }
}
