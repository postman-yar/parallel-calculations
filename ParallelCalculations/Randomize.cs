﻿using System;
using System.Collections.Generic;

namespace ParallelCalculations
{
    public static class Randomize
    {
        public static int[] GetRandomizeArray(int length)
        {
            var rnd = new Random();

            //var arr = new List<int>();
            var arr = new int[length];
            for (var i = 0; i < length; i++)
                arr[i] = rnd.Next(-1_000, 1_000);
                //arr.Add(rnd.Next(-1_000, 1_000));

            return arr;

        }
    }
}
